import os
import os.path
import sys
import torch
import torch.utils.data as data
import cv2
import numpy as np
import yaml
import glob
import os
import ntpath
import json
import random


def get_configs(cgf_file, mode='train'):
    print("Start converting training data......")
    with open(cgf_file, 'r') as stream:
        try:
            configs = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            raise ValueError("Invalid configs.")
    data_sources = configs['datasets'][mode]
    if isinstance(data_sources, str):
        data_sources = [data_sources]

    annotations = []
    for src in data_sources:
        jsons = glob.glob(os.path.join(src, '*.json'))
        print("Find {} jsons in {}.".format(len(jsons), src))
        for jf in jsons:
            image_name = ntpath.basename(jf).rsplit('.', 1)[0]+'.jpg'
            image_path = os.path.join(src, image_name)

            # check file exists
            if not (os.path.exists(jf) and os.path.exists(image_path)):
                print("{} does not exist.".format(image_path))
                continue

            content = json.loads(open(jf).read())
            shapes = content['shapes']
            annotations.append("# "+image_path)
            for shape in shapes:
                points = shape['points']
                x1, y1 = points[0]
                x2, y2 = points[1]
                x1 = int(x1)
                y1 = int(y1)
                x2 = int(x2)
                y2 = int(y2)
                w = x2 - x1
                h = y2 - y1
                if shape['label'] == 'boat':
                    cls = 1
                    annotations.append(
                        "%d %d %d %d -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 0.2 %d" % (
                            x1, y1, w, h, cls))
                else:
                    # import pdb; pdb.set_trace()
                    cls = 0  # BG
                    annotations.append(
                        "%d %d %d %d -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 0.2 %d" % (
                            0, 0, 0, 0, cls))
    print("Converting successfully!")
    return annotations


class WiderFaceDetection(data.Dataset):
    def __init__(self, cgf_file, preproc=None):
        self.preproc = preproc
        self.imgs_path = []
        self.words = []
        lines = get_configs(cgf_file)
        isFirst = True
        labels = []

        for line in lines:
            line = line.rstrip()
            if line.startswith('#'):
                if isFirst is True:
                    isFirst = False
                else:
                    labels_copy = labels.copy()
                    self.words.append(labels_copy)
                    labels.clear()
                path = line[2:]
                self.imgs_path.append(path)
            else:
                line = line.split(' ')
                label = [float(x) for x in line]
                labels.append(label)

        self.words.append(labels)

    def __len__(self):
        return len(self.imgs_path)

    def __getitem__(self, index):
        img = cv2.imread(self.imgs_path[index])
        height, width, _ = img.shape


        labels = self.words[index]
        annotations = np.zeros((0, 15))
        if len(labels) == 0:
            return annotations
        for idx, label in enumerate(labels):
            annotation = np.zeros((1, 15))
            # bbox
            annotation[0, 0] = label[0]  # x1
            annotation[0, 1] = label[1]  # y1
            annotation[0, 2] = label[0] + label[2]  # x2
            annotation[0, 3] = label[1] + label[3]  # y2

            # landmarks
            annotation[0, 4] = label[4]    # l0_x
            annotation[0, 5] = label[5]    # l0_y
            annotation[0, 6] = label[7]    # l1_x
            annotation[0, 7] = label[8]    # l1_y
            annotation[0, 8] = label[10]   # l2_x
            annotation[0, 9] = label[11]   # l2_y
            annotation[0, 10] = label[13]  # l3_x
            annotation[0, 11] = label[14]  # l3_y
            annotation[0, 12] = label[16]  # l4_x
            annotation[0, 13] = label[17]  # l4_y
            if (label[20] == 0):
                annotation[0, 14] = 0
            else:
                annotation[0, 14] = -1

            annotations = np.append(annotations, annotation, axis=0)
        target = np.array(annotations)
        if self.preproc is not None:
            img, target = self.preproc(img, target)

        return torch.from_numpy(img), target

def detection_collate(batch):
    """Custom collate fn for dealing with batches of images that have a different
    number of associated object annotations (bounding boxes).

    Arguments:
        batch: (tuple) A tuple of tensor images and lists of annotations

    Return:
        A tuple containing:
            1) (tensor) batch of images stacked on their 0 dim
            2) (list of tensors) annotations for a given image are stacked on 0 dim
    """
    targets = []
    imgs = []
    for _, sample in enumerate(batch):
        for _, tup in enumerate(sample):
            if torch.is_tensor(tup):
                imgs.append(tup)
            elif isinstance(tup, type(np.empty(0))):
                annos = torch.from_numpy(tup).float()
                targets.append(annos)

    return (torch.stack(imgs, 0), targets)
